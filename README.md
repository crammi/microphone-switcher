# Microphone Switcher

WPF Application to easily change Windows' default microphone using a System Tray Icon and Context Menu

## Installation

To use this program you have to install AudioDeviceCmdlets. Just follow the instructions under https://github.com/frgnca/AudioDeviceCmdlets. After that you just have to execute the .exe

## Usage

After you started the program you will notice a System Tray Icon. A right click on it will show you all your available capturing devices. Left click on a capturing device will set it to Windows' default capturing device.

## License

Open Source
