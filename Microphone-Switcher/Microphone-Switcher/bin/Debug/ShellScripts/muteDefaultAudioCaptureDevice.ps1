$MuteState = Get-AudioDevice -RecordingMute
Write-Output "Audio device Mute State was " $MuteState
if ($MuteState -eq 0) {
   Set-AudioDevice -RecordingMute 1
}  Else {
   Set-AudioDevice -RecordingMute 0
}
$MuteState = Get-AudioDevice -RecordingMute
Write-Output "Audio device Mute State was set to " $MuteState

Read-Host -Prompt "Press Enter to exit"