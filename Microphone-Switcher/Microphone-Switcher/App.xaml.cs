﻿using NAudio.CoreAudioApi;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using Forms = System.Windows.Forms;

namespace Microphone_Switcher
{
    public partial class App : Application
    {
        private Forms.NotifyIcon _notifyIcon;
        private MMDeviceEnumerator _deviceEnumerator;
        private MMDevice _currentDefaultCaptureDevice;       

        public App()
        {
            _notifyIcon = new Forms.NotifyIcon();
            _deviceEnumerator = new MMDeviceEnumerator();
            _currentDefaultCaptureDevice = _deviceEnumerator.GetDefaultAudioEndpoint(DataFlow.Capture, Role.Communications);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            _notifyIcon.Icon = new Icon("Images/microphone.ico");
            _notifyIcon.Text = "Microphone Switcher";
            _notifyIcon.DoubleClick += _notifyIcon_DoubleClick;

            BuildContextMenu();

            _notifyIcon.Visible = true;
        }

        private void BuildContextMenu()
        {
            _notifyIcon.ContextMenuStrip = new Forms.ContextMenuStrip();
            _notifyIcon.ContextMenuStrip.ItemClicked += ContextMenuStrip_ItemClicked;
            MMDeviceCollection devices = getAudioCaptureDevices();
            foreach (MMDevice device in devices)
            {
                Forms.ToolStripItem item = _notifyIcon.ContextMenuStrip.Items.Add(device.FriendlyName);
                item.Tag = device.ID;
                ((Forms.ToolStripMenuItem)item).Checked = ((string)item.Tag).Equals(_currentDefaultCaptureDevice.ID);
            }
        }

        private void ContextMenuStrip_ItemClicked(object sender, Forms.ToolStripItemClickedEventArgs e)
        {
            UncheckAllMenuItems();
            ExecuteShellScript(e.ClickedItem.Text);
            ((Forms.ToolStripMenuItem)e.ClickedItem).Checked = true;
        }

        private void ExecuteShellScript(string name)
        {
            var ps1File = @"ShellScripts\changeAudioDevice.ps1";
            var startInfo = new ProcessStartInfo()
            {
                FileName = "powershell.exe",
                Arguments = $"-NoProfile -WindowStyle Hidden -ExecutionPolicy unrestricted -file \"{ps1File}\" \"{name}\"",
                UseShellExecute = false
            };
            Process.Start(startInfo);
        }

        private void UncheckAllMenuItems()
        {
            foreach (Forms.ToolStripMenuItem item in _notifyIcon.ContextMenuStrip.Items)
            {
                item.Checked = false;
            }
        }
        private MMDeviceCollection getAudioCaptureDevices()
        {
            return _deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
        }

        private void _notifyIcon_DoubleClick(object sender, System.EventArgs e)
        {
            MainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _notifyIcon.Dispose();
            base.OnExit(e);
        }
    }
}
